#!/bin/bash

echo "=== Deploy Stage ==="

echo "=== Build Vue ==="
yarn build
cp ./workshops.json ./dist/workshops.json

echo "=== Sync to Server ==="
rsync -a --delete --progress dist/ hesinde:~/html/miteinandertag/stage
