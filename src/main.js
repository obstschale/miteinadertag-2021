import { createApp } from "vue";
import router from "./router";
import { Remarkable } from "remarkable";
import App from "./App.vue";
import "./index.css";

const remarkable = {
  methods: {
    markdown(text) {
      const md = new Remarkable();
      return md.render(text);
    },
  },
};

createApp(App).use(router).mixin(remarkable).mount("#app");
