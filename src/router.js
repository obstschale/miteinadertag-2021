import { createWebHistory, createRouter } from "vue-router";
import Home from "./layouts/Home.vue";
import Impressum from "./layouts/Impressum.vue";
import Datenschutz from "./layouts/Datenschutz.vue";

const history = createWebHistory();

const routes = [
  { path: "/", component: Home },
  { path: "/impressum", component: Impressum },
  { path: "/datenschutz", component: Datenschutz },
];

const router = createRouter({ history, routes });

export default router;
