#!/bin/bash

echo "=== Deploy Production ==="

echo "=== Build Vue ==="
yarn build

echo "=== Sync to Server ==="
rsync -a --delete --progress dist/ hesinde:~/html/miteinandertag/production
