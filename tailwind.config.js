module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js}"],
  darkMode: false,
  theme: {
    extend: {
      animation: {
        spin: "spin 1s linear 1",
      },
      minHeight: {
        24: "96px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("tailwindcss-debug-screens"),
  ],
};
